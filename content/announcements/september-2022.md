---
title: "Announcing Emacs Asia-Pacific (APAC) virtual meetup, Saturday, September 24, 2022"
linktitle: "September 2022 virtual meetup"
date: 2022-09-10T00:01:37Z
event_date: 2022-09-24T14:00:00+00:00
categories:
- Event
---

This month's [Emacs Asia-Pacific (APAC)](https://emacs-apac.gitlab.io)
virtual meetup is scheduled for Saturday, September
24, 2022 with BigBlueButton and `#emacs`
on Libera Chat IRC. The timing will be [1400 to 1500 IST](# "02:00 PM
Indian Standard Time. UTC+05:30").

*The meetup might get extended by 30 minutes if there is any talk, this
page will be updated accordingly.*

### Talks
- **Haskell Code Exploration with Emacs by Yuchen (~30m)**  
  I will demo how I browse Haskell code and documentation using my
  Emacs package called hc.el and org backend for haddock and briefly
  walk through the code for hc.el. In case people don't know Haskell,
  I'll give a brief and simple introduction to the language needed for
  this demo as well.
  - <https://g.ypei.me/hc.el.git/about/>
  - <https://g.ypei.me/haddock.git/about/>

If you would like to give a demo or talk (maximum 20 minutes) on GNU
Emacs or any variant, please contact `bhavin192` on Libera Chat with
your talk details:

- Topic
- Description
- Duration
- About Yourself

The BigBlueButton (video conferencing) URL for the session:
[https://emacs-apac.gitlab.io/r/bbb/](https://emacs-apac.gitlab.io/r/bbb/
"Redirects to https://bbb.emacsverse.org/b/bha-lms-tqi-dtx")

The URL will also be posted on Libera Chat IRC channels `#emacs`,
`#ilugc` and `#emacsconf`, 30 minutes prior to the meeting, and on the
[ILUGC mailing list](https://www.freelists.org/list/ilugc) on the day
of the meetup. If you are not subscribed, you can also check the
[archive](https://www.freelists.org/archive/ilugc/).
