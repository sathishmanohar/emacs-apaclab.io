---
title: "Announcing Emacs Asia-Pacific (APAC) virtual meetup, Saturday, July 24, 2021"
linktitle: "July 2021 virtual meetup"
date: 2021-07-10T00:31:18Z
event_date: 2021-07-24T14:00:00+00:00
categories:
- Event
---

This month's [Emacs Asia-Pacific (APAC)](https://emacs-apac.gitlab.io)
virtual meetup is scheduled for Saturday, July
24, 2021 with Jitsi Meet and `#emacs` on
Libera Chat IRC. The timing will be [1400 to 1500 IST](# "02:00 PM
Indian Standard Time. UTC+05:30").

*The meetup might get extended by 30 minutes if there is any talk, this
page will be updated accordingly.*

<!-- ### Talks -->
<!-- - **[title] by [speaker] (~20m)**   -->
<!--   [description]. -->

If you would like to give a demo or talk (maximum 20 minutes) on GNU
Emacs or any variant, please contact `bhavin192` on Libera Chat with
your talk details:

- Topic
- Description
- Duration
- About Yourself

The Jitsi Meet (video conferencing) URL for the session will be posted
on Libera Chat IRC channels `#emacs`, `#ilugc` and `#emacsconf`, 30
minutes prior to the meeting, and also on the [ILUGC mailing
list](https://www.freelists.org/list/ilugc) on the day of the
meetup. If you are not subscribed, you can also check the
[archive](https://www.freelists.org/archive/ilugc/).
